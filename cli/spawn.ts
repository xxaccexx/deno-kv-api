const paths: Map<string, string> = new Map();
const isWindows = Deno.build.os === 'windows';
const EOL = isWindows ? '\r\n' : '\n';
const winExt = ['.bat', '.cmd', '.exe'];

const dec = new TextDecoder();

export const spawn = async (command: string) => {
	const [cmd, ...args] = command.split(' ');
	
	if (!paths.has(cmd)) {
		const which = new Deno.Command(
			isWindows ? 'where' : 'which',
			{ args: [cmd] }
		)
		const { code, stdout } = await which.output();
		const raw = dec.decode(stdout);
		const lines: string[] = raw.split(EOL).filter(x => x);

		if (lines.length === 0) throw new Error('Command not found.');

		if (isWindows) {
			const path = lines.find(line => winExt.some(ext => line.includes(ext)));

			if (path) paths.set(cmd, path);
			else throw new Error('Command not found.');

		} else {
			paths.set(cmd, lines[0]);
		}
	}

	const cmdPath = paths.get(cmd) as string;
	
	const child = new Deno.Command(cmdPath, { args });
	const { code, stdout, stderr } = await child.output();

	if (code !== 0) {
		throw new Error(dec.decode(stderr));
	}

	return dec.decode(stdout);
}