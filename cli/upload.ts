import { spawn } from './spawn.ts';

export const upload = (bucket: string, filename: string) => {
	const localname = `./store/${filename}`;
	const remotename = `gs://${bucket}/${filename}`;
	return spawn(`gsutil cp ${localname} ${remotename}`);
}

Deno.test({ name: 'upload', async fn() {
	await upload(
		'pipeline-database',
		'test.txt'
	)
}})