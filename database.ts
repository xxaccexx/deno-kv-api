import { load } from 'std/dotenv/mod.ts'
import { download } from './cli/download.ts';
import { upload } from './cli/upload.ts';

await load({ export: true })

const GCP_BUCKET = Deno.env.get('GCP_BUCKET') ?? 'pipeline-database';
const DB_FILENAME = Deno.env.get('DB_FILENAME') ?? 'journeywise-connector';

try { await download(GCP_BUCKET, DB_FILENAME) }
catch { /* no worries */ }

const kv = await Deno.openKv(`./store/${DB_FILENAME}`);

export class Database <T> {
	private prefix: string;

	constructor(prefix: string) {
		this.prefix = prefix;
	}

	get(key: Deno.KvKey) {
		return kv.get<T>([this.prefix, ...key]);
	}

	async set(key: Deno.KvKey, value: T) {
		const resolution = await kv.atomic()
			.check({ key: [this.prefix, ...key], versionstamp: null })
			.set([this.prefix, ...key], value)
			.commit();

		if (resolution.ok) upload(GCP_BUCKET, DB_FILENAME)

		return resolution;
	}

	async list() {
		const search = await kv.list<T>({ prefix: [this.prefix] });

		const list: T[] = [];
		for await (const item of search) list.push(item.value);

		return list;
	}
}