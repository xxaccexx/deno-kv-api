import { Router } from "oak/mod.ts";
import { Database } from "../database.ts";

export const client = new Router();
const database = new Database<Client>('client');

type Client = {
	id: string;
	name: string;
}

client.get('/client', async ctx => {
	const search = await database.list();

	const list = [];
	for await (const item of search) list.push(item);

	ctx.response.body = list;
})

client.get('/client/:id', async ctx => {
	const result = await database.get([ctx.params.id]);

	if (!result.value) {
		ctx.response.status = 404;
		ctx.response.body = 'Not found';
	} else {
		ctx.response.body = result.value;
	}
})

client.post('/client', async ctx => {
	const client = await ctx.request.body({ type: 'json' }).value as Client
	const commit = await database.set([client.id], client);

	ctx.response.body =  commit;
})

// clients.get('/client/:id/lastFile', async ctx => {

// })