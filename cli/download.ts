import { spawn } from './spawn.ts';

export const download = (bucket: string, filename: string) => {
	const localname = `./store/${filename}`;
	const remotename = `gs://${bucket}/${filename}`;
	return spawn(`gsutil cp ${remotename} ${localname}`);
}

Deno.test({ name: 'upload', async fn() {
	await download(
		'pipeline-database',
		'test.txt'
	)
}})