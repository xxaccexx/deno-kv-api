import { Application, Router } from 'oak';
import { client } from './api/client.ts';

const api = new Router();
api.get('/', ctx => ctx.response.body = 'DB frontend');
api.redirect('/api', '/');
api.use(
	'/api',
	client.routes(),
	client.allowedMethods()
)

const app = new Application();
app.use(api.routes());
app.use(api.allowedMethods());

const running = new Promise(resolve => {
	app.listen({ port: 8080 });
	app.addEventListener('listen', resolve);
})
	.then(() => console.log('Started: http://localhost:8080'))

Deno.test({ name: 'post new item', async fn() {
	await running;

	await fetch('http://localhost:8080/api/client', {
		method: 'POST',
		body: JSON.stringify({
			client: 'test',
			id: Math.round( (Math.random() * 15) ^ 10*15 ).toString(16)
		})
	})
}})